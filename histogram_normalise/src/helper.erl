-module (helper).

% helper functions 

-export ([ceiling/1]).

% function to round up to the nearest whole number
ceiling(X) -> 
     case trunc(X) 
       of Y when Y < X -> Y + 1 ; 
          Z            -> Z 
     end. 
