-module (histogram).

% Given an unordered list, L​, of unknown size that contains integers ranging between 1 and
% 20, write a one arity function, histogram(L)​, to create a histogram, i.e. count the number
% of each value between 1 and 20. The output should be a proplistof length 20 where
% each element takes the form {Index, Count}​. The proplist does not have to be ordered.

% Example: Given the input, L = [5, 1, 2, 2, 4, 5, 5]​, histogram(L)​returns:
% [{1,1},{2,2},{3,0}...{20,0}]

-export ([histogram/1]).

-define (MIN_CHUNK_COUNT, 40000).
-define (DEFAULT_POOL_COUNT, 40).

histogram(L) when is_list(L) ->
	Pids = map(L),
	gather(Pids).

% partition the dataset into chunks, 
% each chunk is processed into a 
% histogram in a new process 
map(L) -> 
	% if smaller than the MIN_CHUNK_COUNT use 
	% one worker else use a fixed worker pool size
	case length(L) of
		ReallySmall when ReallySmall < ?MIN_CHUNK_COUNT -> Chunk = ReallySmall;
		_ 												-> Chunk = helper:ceiling(length(L) / ?DEFAULT_POOL_COUNT) 
	end,
	map(L, Chunk, []).

map([], _, Pids)    -> Pids;
map(L, Chunk, Pids) ->
	{H,T} = lists:split( Chunk,L),
	Pid = launch_histogram_process(H),
	map(T, Chunk, [Pid | Pids]).

% from a list of Pids gather and merges
% the results returning as a list
gather(Pids) ->
	Results = lists:foldl(fun(Pid,Acc) -> 
		receive
			{Pid,Result} ->
				merge(Acc,Result)
		end
	end, empty_dictionary(), Pids),
	dictionary_to_histogram(Results).

launch_histogram_process(L) ->
	Pid = self(),
	spawn(fun() -> Pid ! {self(), calculate_histogram(L)} end ).

calculate_histogram(L) -> 
	lists:foldl(fun(X, Acc) -> dict:update_counter(X, 1, Acc) end, dict:new(), L ).

% helper method to merge two dictionaries
merge(Dic1, Dic2) ->
	dict:merge(fun (_, Value1, Value2) -> Value1 + Value2 end, Dic1, Dic2).

% helper to return a dictionary with a set of default values
empty_dictionary() ->
	lists:foldl(fun(X,Acc) -> dict:store(X, 0, Acc) end, dict:new(), lists:seq(1,20)).

% helper method to convert a dictionary to a histogram
dictionary_to_histogram(Dic) ->
	dict:to_list(Dic).