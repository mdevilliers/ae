% Write a function that takes a list, Histograms,of unknown size where each element is a
% proplist,of length 20, obtained from the above histogram ​function. Write a one arity
% function, normalise(Histograms)​, to combine the histograms into a single histogram
% and normalised into a distribution. The output is a two tuple ​of the new histogram and a
% list representing the distribution.

-module (normalise).

-export ([normalise/1]).
-export ([calculate_normalise/1]).

-define (MIN_CHUNK_COUNT, 10000).
-define (DEFAULT_POOL_COUNT, 100).

normalise(L) when is_list(L) ->
	Pids = map(L),
	Accumulated = gather(Pids),
	calculate_distribution(Accumulated).

% partition the dataset into chunks, 
% each chunk is processed into a 
% histogram in a new process 
map(L) -> 
	% if smaller than the MIN_CHUNK_COUNT use 
	% one worker else use a fixed worker pool size
	case length(L) of
		ReallySmall when ReallySmall < ?MIN_CHUNK_COUNT -> Chunk = ReallySmall;
		_ 												-> Chunk = helper:ceiling(length(L) / ?DEFAULT_POOL_COUNT) 
	end,
	map(L, Chunk, []).

map([], _, Pids) -> Pids;
map(L, Chunk, Pids) ->
	{H,T} = lists:split(Chunk,L),
	Pid = launch_normalise_process(H),
	map(T, Chunk, [Pid | Pids]).

% from a list of Pids merge
% the results returning a histogram
gather(Pids) ->
	Results = lists:foldl( fun(Pid,Acc) -> 
		receive
			{Pid,Result} ->
				merge(Acc,Result)
		end
	end, orddict:new(), Pids),
	dictionary_to_histogram(Results).

calculate_distribution(L) ->
	Total = sum_values(L),
	Distribution = lists:map( fun({_,V})->  V / Total end, L),
	{ L, Distribution}.

launch_normalise_process(L) ->
	Pid = self(),
	spawn(fun() -> 
		Pid ! {self(), calculate_normalise(L)} end ).

calculate_normalise(L) ->
	lists:foldl(fun(X,Acc) -> Dic = histogram_to_dictionary(X), merge(Acc,Dic) end, orddict:new(), L ).

% helper method to merge two ordered dictionaries
merge(Dic1, Dic2) ->
	orddict:merge(fun (_, Value1, Value2) -> Value1 + Value2 end, Dic1, Dic2).

% helper method to convert a ordered dictionary to a histogram
dictionary_to_histogram(Dic) ->
	orddict:to_list(Dic).

% helper method to convert a histogram to an ordered dictionary
histogram_to_dictionary(Histogram) ->
	orddict:from_list(Histogram).

% helper function to total a set of values
sum_values(L) -> 
	lists:foldl(fun({_,V}, Acc) -> Acc + V end, 0, L).
