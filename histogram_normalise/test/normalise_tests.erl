-module (normalise_tests).

-include_lib("eunit/include/eunit.hrl").

basic_usage_test() ->
	Histograms = [ [{1, 32}, {2, 20}, {3, 9}],
	               [{3, 6}, {2, 15}, {1, 18}]],

	{[{1,50},{2,35},{3,15}],[0.5,0.35,0.15]} = normalise:normalise(Histograms).

large_normalise_test_() ->
 	{timeout, 120, [fun large_counts_to_normalise/0 ]}.

large_counts_to_normalise() ->

	Counts = [100,1000,10000,100000,1000000],

	lists:map(fun(Count) ->
		io:format(user ,"~nGenerating test data...", []),
		T = test_data(Count),
		io:format(user ,"~nGenerated test data.", []),
		{Time, _} = timer:tc(normalise, normalise, [T]),
		io:format(user ,"~nCount : ~p, Duration : ~pms", [Count, Time/1000])
	 end, Counts ).

test_data(0) -> [];
test_data(N) -> 
	Values = [{X, random:uniform(20)} || X <- lists:seq(1, 20) ], 
	[Values | test_data(N-1)].
