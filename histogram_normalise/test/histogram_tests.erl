-module (histogram_tests).

-include_lib("eunit/include/eunit.hrl").

basic_usage_test() ->
	L =[5, 1, 2, 2, 4, 5, 5], 
	Unsorted = histogram:histogram(L),
	[{1,1},{2,2},{3,0},{4,1},{5,3},{6,0},{7,0},{8,0},{9,0},{10,0},
	 {11,0},{12,0},{13,0},{14,0},{15,0},{16,0},{17,0},{18,0},{19,0},{20,0}] = sort_histogram(Unsorted).

length_is_twenty_test() ->
	20 = length(histogram:histogram([1])), 
	20 = length(histogram:histogram([1,2,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,1])).

large_histogram_test_() ->
	{timeout, 240, [fun large_histograms/0 ]}.
	
large_histograms()->	
	
	InputLength = 20000, Iterations = 100, 
	io:format(user, "~nChunkSize : ~p, : Iterations : ~p, LargestSize ~p", [InputLength, Iterations, InputLength * Iterations]),

	Values = [random:uniform(20) || _ <- lists:seq(1, InputLength)],

	lists:foldl(fun(_, Acc) -> 
		{Time, Result} = timer:tc(histogram, histogram, [Acc]),
		Checksum = lists:foldl(fun({_,X}, Acc2) -> Acc2 + X end, 0, Result),
		io:format(user, "~nCount : ~p : Time : ~p ms Checksum ~p", [length(Acc), Time/1000, Checksum]),
		lists:append(Acc,Values)		
	end, Values, lists:seq(1,Iterations)).


% helper to sort a histogram
sort_histogram(Histogram) ->
	lists:sort(fun({A,_}, {B,_}) -> A < B end, Histogram).