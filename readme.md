

Excercise 1 and 2 - histogram_normalise
---------------------------------------

```
cd histogram_normalise
rebar compile
rebar eunit

```

Notes

I implemented the methods normalise:normalise/0 and histogram:histogram/0 methods with a map/reduce type solution
after reading the phrase "of unknown size". In each case they inspect the length of the arguments and partition 
the data using a worker pool. The results are then collected and merged into a final result.

The following variables exist in both implementations 

```
-define (MIN_CHUNK_COUNT, 40000).
-define (DEFAULT_POOL_COUNT, 40).
```

MIN_CHUNK_COUNT - defines a minimum length above which to parrelise the workload. 
DEFAULT_POOL_COUNT - defines the size of the worker pool.

The unit tests include more "integration like" tests that generate large amounts of test data. The output to the console 
show the timings of the normalise:normalise/1 and histogram:histogram/1 methods. I added these tests to test that the 
parrelisation helped performance. I output the timings via io:format.


Excercise 3 - timer
-------------------

```
cd timer
rebar get_deps
rebar compile
rebar eunit
```

Notes

I added a dependancy to mockgyver to aid mocking out calls to calendar:universal_time/0.

Excercise 4
-----------

open Question4.pdf

Notes

I actually really enjoyed doing this excercise. 