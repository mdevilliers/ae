-module (helper).

-export ([calculate_remaining_seconds/2, utcnow/0, seconds_to_milliseconds/1]).

% Calculates the remaining timeout period in seconds
calculate_remaining_seconds(From, TimeoutInSeconds) ->
  Now = helper:utcnow(),
  {_, Time} = time_diff(From, Now),
  DiffInSeconds = calendar:time_to_seconds(Time),
  TimeoutInSeconds - DiffInSeconds.

% helper to return the current time in UTC
utcnow() -> calendar:universal_time().

% helper to turn seconds into milliseconds
seconds_to_milliseconds(Seconds) -> Seconds * 1000.

% helper method to diff two calendar timestamps
time_diff(T1, T2) -> calendar:time_difference(T1,T2).
