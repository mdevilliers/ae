-module (timer).

-behaviour(gen_server).

-export([start_link/0]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).

% timeout defined as one hour
-define (TIMEOUT_IN_SECONDS, 60 * 60).

-record (state, {created}).

%% Public API
start_link() ->
  gen_server:start_link(?MODULE, [],[]).

%% Private API
init([]) ->
  
  State = #state{created = helper:utcnow()},
  Timeout = helper:seconds_to_milliseconds( ?TIMEOUT_IN_SECONDS),

  {ok, State, Timeout}.

handle_call(_Request, _From, State) ->

  SecondsRemaining = helper:calculate_remaining_seconds(State#state.created, ?TIMEOUT_IN_SECONDS),
  Timeout = helper:seconds_to_milliseconds(SecondsRemaining),

  {reply, ok, State, Timeout}.

handle_cast(_Msg, State) ->
  
  SecondsRemaining = helper:calculate_remaining_seconds(State#state.created, ?TIMEOUT_IN_SECONDS),
  Timeout = helper:seconds_to_milliseconds(SecondsRemaining),

  {noreply, State, Timeout}.

handle_info(timeout, _) ->
  
  kick:timeout(),
  % i don't know how long this takes - I assume it it less than the timeout period
  % if it takes longer then the implmentation would maybe be
  % spawn(fun() -> 
  %  kick:timeout() 
  % end),
  % but that might not be in the spirit of the excercise?

  State = #state{created = helper:utcnow()},
  Timeout = helper:seconds_to_milliseconds( ?TIMEOUT_IN_SECONDS),

  {noreply, State, Timeout}.

terminate(_Reason, _State) ->
  ok.

code_change(_, State, _) ->
  {ok, State}.