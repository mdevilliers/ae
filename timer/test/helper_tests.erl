-module (helper_tests).

-include_lib("eunit/include/eunit.hrl").
-include_lib("mockgyver/include/mockgyver.hrl").

helper_test_() ->
    ?WITH_MOCKED_SETUP(fun setup/0, fun cleanup/1).

setup() ->
    ok.

cleanup(_) ->
    ok.

timeout_is_correctly_calculated_test() ->
	Now = helper:utcnow(),
	TimeOutInSeconds = 50, 
	TimeOutInSeconds = helper:calculate_remaining_seconds( time_in_past(Now,TimeOutInSeconds), TimeOutInSeconds * 2 ).

timeout_over_day_overlap_test(_) ->
	TenSecondsToMidnight = {{2015,10,10},{23,59,50}},
	FiveSecondsToMidnight = {{2015,10,10},{23,59,55}},
	?WHEN( calendar:universal_time() -> FiveSecondsToMidnight),

	15 = helper:calculate_remaining_seconds(TenSecondsToMidnight, 20).

% this is purely to get 100% code coverage ;-)
seconds_are_correctly_changed_into_milliseconds_test() ->
	1000 = helper:seconds_to_milliseconds(1).

% helper to get time in past
time_in_past(Now, SecondsAgo) -> 
	NowInSeconds = calendar:datetime_to_gregorian_seconds(Now),
	calendar:gregorian_seconds_to_datetime(NowInSeconds - SecondsAgo).
